This module extends Btester CMS by adding a portfolio content type.

Adds the following settings:

* Adds a new portfolio content type (bt_portfolio)
* Create a path alias to edit page of every bt_portfolio node, from /node/$entity_id/edit to /portfolio/$path_title/edit
* Create a path alias to delete page of every bt_portfolio node, from /node/$entity_id/delete to /portfolio/$path_title/delete
* Create path alias to create portfolio page, from /app/website/content/create/bt_portfolio to /app/website/content/create/portfolio
* Adds a page_manager page /portfolio
* Adds a link to /portfolio in main menu
* Adds the corresponding permissions to the roles defined by Btester CMS.
* Adds a block content type portfolio (bt_portfolio)
* Adds a taxonomy vocabulary
* Adds views
