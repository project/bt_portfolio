<?php

namespace Drupal\bt_portfolio\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\page_manager\Entity\Page;

/**
 * Provides menu links of all Node entities with type "Portfolio".
 */
class IpePageMenuLinkDerivative extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $links = array();
    $page = Page::load('ipe_portfolio');
    if ($page->status()) {
      $links['main_' . $page->id()] = [
        'title' => $page->label(),
        'description' => 'Portfolio of works',
        'menu_name' => 'main',
        'weight' => 5,
        'route_name' => 'page_manager.page_view_ipe_portfolio_ipe_portfolio-panels_variant-0',
      ] + $base_plugin_definition;
    }

    return $links;
  }

}
