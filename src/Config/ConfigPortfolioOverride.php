<?php

namespace Drupal\bt_portfolio\Config;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryOverrideInterface;
use Drupal\Core\Config\ConfigFactory;

/**
 * {@inheritdoc}
 */
class ConfigPortfolioOverride implements ConfigFactoryOverrideInterface {

  private $createContent;
  private $deleteContent;
  private $deleteOwnContent;
  private $editContent;
  private $viewsAdminContent;
  private $viewsFullAdminContent;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactory $configFactory) {
    $this->createContent = $configFactory->get('user.role.bt_create_content');
    $this->deleteContent = $configFactory->get('user.role.bt_delete_content');
    $this->deleteOwnContent = $configFactory->get('user.role.bt_delete_own_content');
    $this->editContent = $configFactory->get('user.role.bt_edit_publish_content');
    $this->viewsAdminContent = $configFactory->get('views.view.bt_admin_content');
    $this->viewsFullAdminContent = $configFactory->get('views.view.bt_full_admin_content');
  }

  /**
   * {@inheritdoc}
   */
  public function loadOverrides($names) {
    $overrides = array();

    // Add portfolio permissions to bt_create_content role.
    if (in_array('user.role.bt_create_content', $names)) {
      $portfolio_permissions = [
        'create bt_portfolio content',
        'edit own bt_portfolio content',
        'revert bt_portfolio revisions',
        'view bt_portfolio revisions',
      ];
      $content_role = $this->createContent;
      $permissions = array_merge($content_role->get('permissions'), $portfolio_permissions);
      $overrides['user.role.bt_create_content']['permissions'] = $permissions;
    }
    // Add portfolio permissions to bt_delete_content role.
    if (in_array('user.role.bt_delete_content', $names)) {
      $portfolio_permissions = [
        'delete any bt_portfolio content',
        'delete bt_portfolio revisions',
      ];
      $content_role = $this->deleteContent;
      $permissions = array_merge($content_role->get('permissions'), $portfolio_permissions);
      $overrides['user.role.bt_delete_content']['permissions'] = $permissions;
    }
    // Add portfolio permissions to bt_delete_own_content role.
    if (in_array('user.role.bt_delete_own_content', $names)) {
      $portfolio_permissions = [
        'delete own bt_portfolio content',
      ];
      $content_role = $this->deleteOwnContent;
      $permissions = array_merge($content_role->get('permissions'), $portfolio_permissions);
      $overrides['user.role.bt_delete_own_content']['permissions'] = $permissions;
    }
    // Add portfolio permissions to bt_edit_publish_content role.
    if (in_array('user.role.bt_edit_publish_content', $names)) {
      $portfolio_permissions = [
        'edit any bt_portfolio content',
      ];
      $content_role = $this->editContent;
      $permissions = array_merge($content_role->get('permissions'), $portfolio_permissions);
      $overrides['user.role.bt_edit_publish_content']['permissions'] = $permissions;
    }
    $portfolio_values = [
      'bt_portfolio' => 'bt_portfolio',
    ];
    // Add portfolio filter values to views.view.bt_admin_content view.
    if (in_array('views.view.bt_admin_content', $names)) {
      $views = $this->viewsAdminContent;
      $filter_values = $views->get('display.default.display_options.filters.type.value');
      $values = array_merge($filter_values, $portfolio_values);
      $overrides['views.view.bt_admin_content']['display']['default']['display_options']['filters']['type']['value'] = $values;
      $overrides['views.view.bt_admin_content']['display']['default']['display_options']['filters']['type_expose']['value'] = $values;
    }
    // Add portfolio filter values to views.view.bt_full_admin_content view.
    if (in_array('views.view.bt_full_admin_content', $names)) {
      $views = $this->viewsFullAdminContent;
      $filter_values = $views->get('display.default.display_options.filters.type.value');
      $values = array_merge($filter_values, $portfolio_values);
      $overrides['views.view.bt_full_admin_content']['display']['default']['display_options']['filters']['type']['value'] = $values;
      $overrides['views.view.bt_full_admin_content']['display']['default']['display_options']['filters']['type_expose']['value'] = $values;
    }

    return $overrides;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheSuffix() {
    return 'ConfigPortfolioOverride';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($name) {
    return new CacheableMetadata();
  }

  /**
   * {@inheritdoc}
   */
  public function createConfigObject($name, $collection = StorageInterface::DEFAULT_COLLECTION) {
    return NULL;
  }

}
